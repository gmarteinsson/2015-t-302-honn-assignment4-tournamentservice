package is.rufan.tournament.test;

import is.rufan.tournament.domain.Tournaments;
import is.rufan.tournament.service.TournamentsService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import java.util.Date;

public class Test {
    protected ApplicationContext ctx = new FileSystemXmlApplicationContext("/src/main/resources/tournamentsapp.xml");

    public static void main(String[] args) {
        new Test();
    }

    Test() {
        TournamentsService tournamentsService = (TournamentsService) ctx.getBean("tournamentsService");
        tournamentsService.getTournaments();

        /*
        TournamentsService tournamentsService = (TournamentsService) ctx.getBean("tournamentsService");

        Tournaments testTournament = new Tournaments();

        Date sdate = new Date(2012, 6,18,10,34,9);
        Date edate = new Date(2015, 8, 15, 21, 00);

        testTournament.setTournamentid(112389);
        testTournament.setTournamentname("Monster");
        testTournament.setStarttime(sdate);
        testTournament.setEndtime(edate);
        testTournament.setLeaguename("EPL");

        tournamentsService.addTournaments(testTournament);
        */
    }
}
