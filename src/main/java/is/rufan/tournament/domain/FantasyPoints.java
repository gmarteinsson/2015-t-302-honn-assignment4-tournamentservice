package is.rufan.tournament.domain;

public class FantasyPoints {
    private int playerId;
    private double fantasyPoints;

    public FantasyPoints()
    {
    }

    public FantasyPoints(int playerId, int fantasyPoints) {
        this.playerId = playerId;
        this.fantasyPoints = fantasyPoints;
    }

    public double getFantasyPoints() { return fantasyPoints; }

    public void setFantasyPoints(double fantasyPoints) { this.fantasyPoints = fantasyPoints; }

    public int getPlayerId() { return playerId; }

    public void setPlayerId(int playerId) { this.playerId = playerId; }
}
