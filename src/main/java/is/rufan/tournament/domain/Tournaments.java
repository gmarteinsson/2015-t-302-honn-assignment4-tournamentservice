package is.rufan.tournament.domain;

import java.util.Date;

public class Tournaments {
    private int tournamentid;
    private String tournamentname;
    private Date starttime;
    private Date endtime;
    private String leaguename;

    public Tournaments() {
    }

    public Tournaments(int tournamentid, String tournamentname, Date starttime, Date endtime, String leaguename) {
        this.tournamentid = tournamentid;
        this.tournamentname = tournamentname;
        this.starttime = starttime;
        this.endtime = endtime;
        this.leaguename = leaguename;
    }

    public int getTournamentid() {
        return tournamentid;
    }

    public void setTournamentid(int tournamentid) {
        this.tournamentid = tournamentid;
    }

    public String getTournamentname() {
        return tournamentname;
    }

    public void setTournamentname(String tournamentname) {
        this.tournamentname = tournamentname;
    }

    public Date getStarttime() {
        return starttime;
    }

    public void setStarttime(Date starttime) {
        this.starttime = starttime;
    }

    public Date getEndtime() {
        return endtime;
    }

    public void setEndtime(Date endtime) {
        this.endtime = endtime;
    }

    public String getLeaguename() {
        return leaguename;
    }

    public void setLeaguename(String leaguename) {
        this.leaguename = leaguename;
    }
}
