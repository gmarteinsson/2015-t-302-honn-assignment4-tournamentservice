package is.rufan.tournament.process;

import is.rufan.tournament.domain.FantasyPoints;
import is.rufan.tournament.service.FantasyPoinsServiceException;
import is.rufan.tournament.service.FantasyPointsService;
import is.ruframework.process.RuAbstractProcess;
import is.ruframework.reader.RuReadHandler;
import is.ruframework.reader.RuReader;
import is.ruframework.reader.RuReaderException;
import is.ruframework.reader.RuReaderFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import java.util.logging.Logger;

public class FantasyPointsImportProcess extends RuAbstractProcess implements RuReadHandler
{
    private FantasyPointsService fpService;
    MessageSource msg;
    Logger logger = Logger.getLogger(this.getClass().getName());

    @Override
    public void beforeProcess() {
        ApplicationContext applicationContext = new FileSystemXmlApplicationContext("classpath:fantasypointsapp.xml");
        msg = (MessageSource)applicationContext.getBean("messageSource");
        logger.info("processbefore; " + getProcessContext().getProcessName());
        fpService = (FantasyPointsService)applicationContext.getBean("fantasypointsService");
    }

    @Override
    public void startProcess()
    {
        RuReaderFactory factory = new RuReaderFactory("fantasypointsprocess.xml");
        RuReader reader = factory.getReader("fantasypointsReader");

        reader.setReadHandler(this);
        try
        {
            reader.read();
        }
        catch (RuReaderException e)
        {
            String errorMsg = "Unable to read specified file.";
            logger.severe(errorMsg);
        }
    }

    public void read (int count, Object object)
    {
        FantasyPoints points = (FantasyPoints)object;

        try
        {
            fpService.addFantasyPoints(points);
        }
        catch(FantasyPoinsServiceException fe)
        {
            logger.warning("FantasyPoints not added " + fe.getMessage());
        }
    }
}
