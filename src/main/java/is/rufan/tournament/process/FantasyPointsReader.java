package is.rufan.tournament.process;

import is.rufan.tournament.domain.FantasyPoints;
import is.ruframework.reader.RuAbstractReader;
import is.ruframework.reader.RuJsonUtil;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.util.ArrayList;
import java.util.List;

public class FantasyPointsReader extends RuAbstractReader
{
    public Object parse(String content)
    {
        String tmp;
        FantasyPoints point;

        List<FantasyPoints> fantasypoints = new ArrayList<FantasyPoints>();

        //Root object
        JSONArray fPoints = (JSONArray)JSONValue.parse(content);

        // Iterate through the array, and display points.
        JSONObject jpoint;

        for(int i = 0; i < fPoints.size(); i++)
        {
            point = new FantasyPoints();
            jpoint = (JSONObject)fPoints.get(i);
            //point.setFantasyPoints((Long) jpoint.get("FantasyPoints"));
            point.setFantasyPoints(((Number) jpoint.get("FantasyPoints")).doubleValue());

            point.setPlayerId(RuJsonUtil.getInt(jpoint, "PlayerId"));

            fantasypoints.add(point);
            readHandler.read(i, point);
        }

        return fantasypoints;
    }
}
