package is.rufan.tournament.service;

public class TournamentsServiceException extends RuntimeException {
    public TournamentsServiceException() {
    }

    public TournamentsServiceException(String message) {
        super(message);
    }

    public TournamentsServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}