package is.rufan.tournament.service;


import is.rufan.tournament.data.TournamentsDataGateway;
import is.rufan.tournament.domain.Tournaments;
import is.ruframework.data.RuDataAccessFactory;
import is.ruframework.domain.RuException;

import java.util.List;

public class TournamentsServiceData implements TournamentsService {
    RuDataAccessFactory factory;
    TournamentsDataGateway tournamentsDataGateway;

    public TournamentsServiceData() throws RuException
    {
        factory = RuDataAccessFactory.getInstance("tournamentsdata.xml");
        tournamentsDataGateway = (TournamentsDataGateway) factory.getDataAccess("tournamentsData");
    }

    public void addTournaments(Tournaments tournaments) throws TournamentsServiceException
    {
        tournamentsDataGateway.addTournament(tournaments);
    }

    public List<Tournaments> getTournaments()
    {
       return tournamentsDataGateway.find();
    }
}