package is.rufan.tournament.service;

import is.rufan.tournament.domain.Tournaments;

import java.util.List;

public interface TournamentsService {
    public void addTournaments(Tournaments tournaments) throws TournamentsServiceException;
    public List<Tournaments> getTournaments();
}

