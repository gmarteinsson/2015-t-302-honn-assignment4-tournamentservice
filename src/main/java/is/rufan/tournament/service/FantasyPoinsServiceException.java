package is.rufan.tournament.service;

public class FantasyPoinsServiceException extends RuntimeException
{
    public FantasyPoinsServiceException()
    {
    }

    public FantasyPoinsServiceException(String message) { super(message); }

    public FantasyPoinsServiceException(String message, Throwable cause) { super(message, cause); }
}
