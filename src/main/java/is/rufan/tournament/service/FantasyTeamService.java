package is.rufan.tournament.service;

import is.rufan.tournament.domain.FantasyTeam;

import java.util.List;

public interface FantasyTeamService {
    public FantasyTeam getFantasyTeams(int userid);
}
