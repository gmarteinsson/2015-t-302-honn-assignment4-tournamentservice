package is.rufan.tournament.service;

import is.rufan.tournament.domain.FantasyPoints;

public interface FantasyPointsService {
    public void addFantasyPoints(FantasyPoints points) throws FantasyPoinsServiceException;
}
