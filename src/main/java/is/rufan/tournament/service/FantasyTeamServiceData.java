package is.rufan.tournament.service;


import is.rufan.tournament.data.FantasyTeamDataGateway;
import is.rufan.tournament.domain.FantasyTeam;
import is.ruframework.data.RuDataAccessFactory;
import is.ruframework.domain.RuException;

import java.util.List;

public class FantasyTeamServiceData implements FantasyTeamService{
    RuDataAccessFactory factory;
    FantasyTeamDataGateway fantasyTeamDataGateway;

    public FantasyTeamServiceData() throws RuException
    {
        factory = RuDataAccessFactory.getInstance("fantasyteamdata.xml");
        fantasyTeamDataGateway = (FantasyTeamDataGateway) factory.getDataAccess("fantasyteamdata");
    }

    public FantasyTeam getFantasyTeams(int userid)
    {
        return fantasyTeamDataGateway.listofTeams(userid);
    }
}
