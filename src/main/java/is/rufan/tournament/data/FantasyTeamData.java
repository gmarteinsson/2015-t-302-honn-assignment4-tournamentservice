package is.rufan.tournament.data;

import is.rufan.tournament.domain.FantasyTeam;
import is.ruframework.data.RuData;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

public class FantasyTeamData extends RuData implements FantasyTeamDataGateway {


    public FantasyTeam listofTeams(int userid)
    {
        String sql = "select tm.tournamentid, tm.tournamentname, tm.starttime, tm.endtime, tm.leaguename, ft.fantasyteamid, ft.fantasyteamname, ft.userid from tournaments tm JOIN fantasyteams ft ON tm.tournamentid = ft.tournamentid where ft.userid = ?";

        JdbcTemplate queryTeams = new JdbcTemplate(getDataSource());

        try
        {
             FantasyTeam teams = (FantasyTeam)queryTeams.queryForObject(sql, new Object[]{userid},
                     new FantasyTeamRowMapper());

            System.out.println("teams in tournament service" + teams);
            return teams;
        }
        catch (EmptyResultDataAccessException erdax)
        {
            return null;
        }
    }
}
