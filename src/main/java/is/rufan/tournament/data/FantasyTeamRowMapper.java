package is.rufan.tournament.data;

import is.rufan.tournament.domain.FantasyTeam;

import org.springframework.jdbc.core.RowMapper;
import java.sql.ResultSet;
import java.sql.SQLException;

public class FantasyTeamRowMapper implements RowMapper<FantasyTeam> {
    public FantasyTeam mapRow(ResultSet rs, int rowNum) throws SQLException
    {
        FantasyTeam f = new FantasyTeam();
        f.setFantasyteamid(rs.getInt("fantasyteamid"));
        f.setFantasyteamname(rs.getString("fantasyteamname"));
        f.setUserid(rs.getInt("userid"));
        f.setTournamentid(rs.getInt("tournamentid"));
        f.setTournamentname(rs.getString("tournamentname"));
        f.setStarttime(rs.getDate("starttime"));
        f.setEndtime(rs.getDate("endtime"));
        f.setLeaguename(rs.getString("leaguename"));

        return f;
    }
}
