package is.rufan.tournament.data;

import is.rufan.tournament.domain.Tournaments;
import is.ruframework.data.RuData;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TournamentsData extends RuData implements TournamentsDataGateway {
    public void addTournament(Tournaments tournaments)
    {
        SimpleJdbcInsert insertTournaments =
                new SimpleJdbcInsert(getDataSource())
                        .withTableName("tournaments");

        Map<String, Object> tournamentsParameters = new HashMap<String, Object>(5);
        tournamentsParameters.put("tournamentid", tournaments.getTournamentid());
        tournamentsParameters.put("tournamentname", tournaments.getTournamentname());
        tournamentsParameters.put("starttime", tournaments.getStarttime());
        tournamentsParameters.put("endtime", tournaments.getEndtime());
        tournamentsParameters.put("leaguename", tournaments.getLeaguename());

        try {
            insertTournaments.execute(tournamentsParameters);
        }
        catch (DataIntegrityViolationException divex) {
            log.warning("Dublicate entry");
        }
    }

    public List<Tournaments> find()
    {
        String sql = "select * from tournaments where starttime > GETDATE()";
        JdbcTemplate queryTour = new JdbcTemplate((getDataSource()));
        try
        {
            List<Tournaments> tournament = (List<Tournaments>)queryTour.query(sql, new TournamentsRowMapper());
            return tournament;
        }
        catch (EmptyResultDataAccessException erdax)
        {
            return null;
        }
    }
}
