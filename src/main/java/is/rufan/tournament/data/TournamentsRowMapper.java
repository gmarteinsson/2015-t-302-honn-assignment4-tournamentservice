package is.rufan.tournament.data;


import is.rufan.tournament.domain.Tournaments;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TournamentsRowMapper implements RowMapper<Tournaments> {
    public Tournaments mapRow(ResultSet rs, int rowNum) throws SQLException
    {
        Tournaments t = new Tournaments();
        t.setTournamentid(rs.getInt("tournamentid"));
        t.setTournamentname(rs.getString("tournamentname"));
        t.setStarttime(rs.getDate("starttime"));
        t.setEndtime(rs.getDate("endtime"));
        t.setLeaguename(rs.getString("leaguename"));

        return t;
    }

}