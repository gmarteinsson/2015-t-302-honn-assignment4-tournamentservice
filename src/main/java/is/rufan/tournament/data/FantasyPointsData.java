package is.rufan.tournament.data;

import is.rufan.tournament.domain.FantasyPoints;
import is.ruframework.data.RuData;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import java.util.HashMap;
import java.util.Map;

public class FantasyPointsData extends RuData implements FantasyPointsDataGateway
{
    public void addFantasyPoints(FantasyPoints points)
    {
        SimpleJdbcInsert insertPoints =
                new SimpleJdbcInsert(getDataSource())
                .withTableName("fantasypoints");

        Map<String, Object> fantasyPointsParameters = new HashMap<String, Object>(2);
        fantasyPointsParameters.put("playerid", points.getPlayerId());
        fantasyPointsParameters.put("fantasypoints", points.getFantasyPoints());

        try
        {
            insertPoints.execute(fantasyPointsParameters);
        }
        catch (DataIntegrityViolationException divex)
        {
            log.warning("Dublicate entry");
        }

    }
}
