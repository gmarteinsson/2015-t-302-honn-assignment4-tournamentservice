package is.rufan.tournament.data;

import is.rufan.tournament.domain.FantasyPoints;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class FantasyPointsRowMapper implements RowMapper<FantasyPoints>
{
    public FantasyPoints mapRow(ResultSet rs, int rowNum) throws SQLException
    {
        FantasyPoints fp = new FantasyPoints();
        fp.setPlayerId(rs.getInt("playerid"));
        fp.setFantasyPoints(rs.getInt("fantasypoints"));

        return fp;
    }
}
