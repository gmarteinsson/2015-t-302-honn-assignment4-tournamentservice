package is.rufan.tournament.data;

import is.rufan.tournament.domain.FantasyPoints;

public interface FantasyPointsDataGateway
{
    public void addFantasyPoints(FantasyPoints points);
}
