package is.rufan.tournament.data;

import is.rufan.tournament.domain.FantasyTeam;

import java.util.List;

public interface FantasyTeamDataGateway {
    public FantasyTeam listofTeams(int userid);
}
