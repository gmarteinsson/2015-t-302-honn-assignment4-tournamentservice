package is.rufan.tournament.data;

import is.rufan.tournament.domain.Tournaments;

import java.util.List;

public interface TournamentsDataGateway
{
    public void addTournament(Tournaments tournaments);
    public List<Tournaments> find();
}
