
drop table fantasyteams

create table fantasyteams
(
  fantasyteamid int primary key NOT NULL,
  fantasyteamname varchar(50),
  userid int,
  tournamentid int
)
