
drop table tournaments

create table tournaments
(
  tournamentid int primary key NOT NULL,
  tournamentname varchar(50),
  starttime datetime,
  endtime datetime,
  leaguename varchar(50)
)